module.exports = {
	publicPath: (process.env.NODE_ENV === 'production' ? '/time-tracking/' : '/'),
	pwa: {
		name: 'Time Tracker',
		themeColor: '#343a40',
		msTileColor: '#343a40'
	},
	pages: {
		index: {
			entry: 'src/main.js',
			title: 'Time Tracker'
		}
	},
	lintOnSave: false
};
