# Time Tracker

This is a little tool for everyone who needs good and free time tracking over his tasks

Website: https://crazy252.gitlab.io/time-tracking/

## Project setup
Install all dependencies and setup project
```
npm install
```
Compiles and hot-reloads for development
```
npm run serve
```
Compiles and minifies for production
```
npm run build
```
Run your unit tests
```
npm run test:unit
```

## Upcoming features
- Translations for other languages
- Mobile design
