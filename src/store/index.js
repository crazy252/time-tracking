import Vue from 'vue'
import Vuex from 'vuex'
import {v4 as uuidv4} from 'uuid'
import eventBus from '../util/eventBus'

import _find from 'lodash/find'
import _filter from 'lodash/filter'
import _orderBy from 'lodash/orderBy'
import _last from 'lodash/last'
import _isEmpty from 'lodash/isEmpty'
import _findIndex from 'lodash/findIndex'
import _sortBy from 'lodash/sortBy'

Vue.use(Vuex)

const localStorageTimersKey = 'tt_timers'
const localStorageSettingsKey = 'tt_settings'

const defaultSettings = {
	sort: 'desc'
}

export default new Vuex.Store({
	state: {
		timers: (JSON.parse(localStorage.getItem(localStorageTimersKey) ?? '[]')),
		settings: Object.assign(defaultSettings, JSON.parse(localStorage.getItem(localStorageSettingsKey) ?? '{}'))
	},
	getters: {
		// get all timers
		timers: (state) => {
			return state.timers
		},
		// get settings
		settings: (state) => {
			return state.settings
		},

		timersNormalized: state => {
			return JSON.parse(JSON.stringify(state.timers))
		},
		// amount of timers
		timersCount: (state) => {
			return JSON.parse(JSON.stringify(state.timers)).length
		},
		// get list of timers sorted by order / default: favourite === false
		timersList: (state, getters) => (favouriteStatus = false) => {
			// filter timers by favourite
			let timers = _filter(getters.timersNormalized, item => item.favourite === favouriteStatus)
			// sort timers by setting "order"
			return _orderBy(timers, ['name'], [state.settings.sort])
		},
		// get timer or an empty object
		timer: (state, getters) => (uuid) => {
			// filter timers array by uuid
			let timer = _find(getters.timersNormalized, item => item.uuid === uuid)

			// if undefined -> no timer found
			if (timer === undefined)
				return {}

			// timer found - return timer object
			return timer
		},

		// get sprint duration of timer
		timerSprint: (state, getters) => (uuid, timestamp = false) => {
			// get timer
			let timer = getters.timer(uuid)

			// check if timer found
			if (_isEmpty(timer))
				return (timestamp ? 0 : '0:00')

			// check amount of records
			if (timer.records.length === 0)
				return (timestamp ? 0 : '0:00')

			// get latest record
			let latest = _last(_sortBy(timer.records, ['index']))

			// calc duration
			let duration = parseInt(new Date().getTime() / 1000) - latest.start
			if (!timer.running)
				duration = latest.end - latest.start

			return (timestamp ? duration : getters.formatTimestamp(duration))
		},
		// get total duration of timer
		timerTotal: (state, getters) => (uuid, timestamp = false) => {
			// get timer
			let timer = getters.timer(uuid)

			// check if timer found
			if (_isEmpty(timer))
				return (timestamp ? 0 : '0:00')

			let duration = 0

			for (let index = 0; index < timer.records.length; index++) {

				let record = timer.records[index]

				let end = parseInt(new Date().getTime() / 1000)
				if (record.end !== null)
					end = record.end

				duration += (end - record.start)

			}

			return (timestamp ? duration : getters.formatTimestamp(duration))
		},
		// get total duration
		timersTotal: (state, getters) => (timestamp = false) => {
			let duration = 0, uuidList = getters.timersNormalized.map(item => item.uuid)

			for (let index = 0; index < uuidList.length; index++)
				duration += getters.timerTotal(uuidList[index], true)

			// return duration as timestamp
			if (timestamp)
				return duration

			// return duration formatted
			return getters.formatTimestamp(duration)
		},

		// get status of field from timer
		timerStatus: (state, getters) => (uuid, field) => {
			// get timer
			let timer = getters.timer(uuid)

			// check if timer found
			if (_isEmpty(timer))
				return false

			// field not found - return false
			if (!(field in Object.keys(timer)))
				return false

			// return status of field
			return timer[field]
		},

		// get records of timer
		timerRecords: (state, getters) => uuid => {
			// get timer
			let timer = getters.timer(uuid)

			// check if timer found
			if (_isEmpty(timer))
				return []

			// check if records is empty
			if (_isEmpty(timer.records))
				return []

			// return reversed array of records - newest is at the top
			return timer.records.reverse()
		},

		settingSort: (state) => {
			return state.settings.sort
		},

		// format timestamp to date
		formatTimestamp: () => (timestamp) => {
			// check for 0 timestamp
			if (timestamp === 0)
				return '0:00'

			// new date object
			let date = new Date()

			// set timestamp to milliseconds
			date.setTime(timestamp * 1000)

			// output array
			let output = []

			// hours
			if (date.getUTCHours() > 0)
				output.push(date.getUTCHours())

			// minutes
			output.push(date.getUTCMinutes() > 9 ? date.getUTCMinutes() : date.getUTCMinutes())

			// seconds
			output.push((date.getUTCSeconds() > 9 ? date.getUTCSeconds() : '0' + date.getUTCSeconds()))

			// fallback if output is empty
			if (_isEmpty(output))
				return '0:00'

			// join array
			return output.join(':')
		}
	},
	mutations: {
		// add timer
		timerAdd(state, name) {
			// make a uuid v4
			let uuid = uuidv4().split('-').join('')

			// normalize timers array
			let timers = JSON.parse(JSON.stringify(state.timers))

			// set timer to states
			let timer = {
				name: name,
				uuid: uuid,
				running: false,
				expand: false,
				favourite: false,
				recordsIndex: 0,
				records: []
			}

			// add timer and set timers
			timers.push(timer)
			state.timers = timers
		},
		// remove timer
		timerRemove(state, uuid) {
			// list of timers
			let timers = JSON.parse(JSON.stringify(state.timers))

			// get index of timer
			let index = _findIndex(timers, timer => timer.uuid === uuid)
			// check if index is below 0 (-1 is not found) and exit with true
			if (index < 0)
				return true

			// delete timer
			timers.splice(index, 1)

			// set list of timers
			state.timers = timers
		},

		// start timer
		timerStart(state, uuid) {
			// list of timers
			let timers = JSON.parse(JSON.stringify(state.timers))

			// get index of timer -> below 0 is not found
			let index = _findIndex(timers, timer => timer.uuid === uuid)
			if (index < 0)
				return true

			// get timer by index
			let timer = timers[index]

			// make new record
			let record = {
				index: timer.recordsIndex,
				start: parseInt(new Date().getTime() / 1000),
				comment: '',
				end: null
			}

			// set running status and record
			timer.running = true
			timer.records.push(record)
			timer.recordsIndex = timer.recordsIndex + 1

			// set timer and timers
			timers[index] = timer
			state.timers = timers
		},
		// stop timer
		timerStop(state, uuid = null) {
			// list of timers
			let timers = JSON.parse(JSON.stringify(state.timers))

			// get index of timer running status
			let index = _findIndex(timers, timer => timer.running)
			// get index of timer by uuid
			if (uuid !== null)
				index = _findIndex(timers, timer => timer.uuid === uuid)

			// check if index is below 0 (-1 is not found) and exit with true
			if (index < 0)
				return true

			// get timer by index
			let timer = timers[index]
			// get last record index by length
			let recordIndex = timer.records.length - 1

			// set running status and record end
			timer.running = false
			timer.records[recordIndex].end = parseInt(new Date().getTime() / 1000)

			// set timer and timers
			timers[index] = timer
			state.timers = timers
		},
		// change name
		timerName(state, payload) {
			// list of timers
			let timers = JSON.parse(JSON.stringify(state.timers))

			// get index of timer -> below 0 is not found
			let index = _findIndex(timers, timer => timer.uuid === payload.uuid)
			if (index < 0)
				return true

			// get timer by index
			let timer = timers[index]

			// set new name
			timer.name = payload.name

			// set timer and timers
			timers[index] = timer
			state.timers = timers
		},

		// reset all sprints of timer
		timerSprintRemoveAll(state, uuid) {
			// list of timers
			let timers = JSON.parse(JSON.stringify(state.timers))

			// get index of timer -> below 0 is not found
			let index = _findIndex(timers, timer => timer.uuid === uuid)
			if (index < 0)
				return true

			// get timer by index
			let timer = timers[index]

			// stop timer if running
			if (timer.running)
				timer.running = false

			// remove all record
			timer.records = []

			// set timer and timers
			timers[index] = timer
			state.timers = timers
		},
		// remove a sprint from the timer
		timerSprintRemoveIndex(state, payload) {
			// list of timers
			let timers = JSON.parse(JSON.stringify(state.timers))

			// get index of timer -> below 0 is not found
			let index = _findIndex(timers, timer => timer.uuid === payload.uuid)
			if (index < 0)
				return true

			// get timer by index
			let timer = timers[index]

			// get records, get last record and index of removing record
			let records = _sortBy(timer.records, ['index'])
			let recordLast = _last(records)
			let recordIndex = _findIndex(records, record => record.index === payload.index)
			if (index < 0)
				return true

			// stop timer if running and the position is the latest record
			if (timer.running && recordLast.index === recordIndex)
				timer.running = false

			// drop record and set it back into timer
			records.splice(recordIndex, 1)
			timer.records = records

			// set timer and timers
			timers[index] = timer
			state.timers = timers
		},
		// remove latest sprint from the timer
		timerSprintRemoveLatest(state, uuid) {
			// list of timers
			let timers = JSON.parse(JSON.stringify(state.timers))

			// get index of timer -> below 0 is not found
			let index = _findIndex(timers, timer => timer.uuid === uuid)
			if (index < 0)
				return true

			// get timer by index
			let timer = timers[index]

			// stop timer if running
			if (timer.running)
				timer.running = false

			// get records, drop last and set it back into timer
			let records = _sortBy(timer.records, ['index'])
			records.pop()
			timer.records = records

			// set timer and timers
			timers[index] = timer
			state.timers = timers
		},

		// toggle expand/favourite status
		timerStatusToggle(state, payload) {
			// list of timers
			let timers = JSON.parse(JSON.stringify(state.timers))
			// payload: uuid, field
			let uuid = payload.uuid, field = payload.field

			// get index of timer
			let index = _findIndex(timers, timer => timer.uuid === uuid)
			// check if index is below 0 (-1 is not found) and exit with true
			if (index < 0)
				return true

			// get timer by index
			let timer = timers[index]

			// set inverted status of field
			timer[field] = !timer[field]

			// set timer and timers
			timers[index] = timer
			state.timers = timers
		},

		// update comment of record
		timerUpdateComment(state, payload) {
			// list of timers
			let timers = JSON.parse(JSON.stringify(state.timers))

			// get timer index by uuid
			let timerIndex = _findIndex(timers, timer => timer.uuid === payload.uuid)
			// check if index is below 0 (-1 is not found) and exit with true
			if (timerIndex < 0)
				return true

			// get result of filter
			let timer = timers[timerIndex]

			// get record index by index inside of record
			let recordIndex = _findIndex(timer.records, record => record.index === payload.index)
			// check if index is below 0 (-1 is not found) and exit with true
			if (recordIndex < 0)
				return true

			// set comment to record
			timer.records[recordIndex].comment = payload.comment

			// set timer and timers
			timers[timerIndex] = timer
			state.timers = timers
		},

		// change sort setting
		settingSortChange(state, sort) {
			state.settings.sort = sort
		},

		// update timers 
		update(state, payload) {
			// getters and title
			let getters = payload.get, title = ''

			// set total timer text
			let elTotal = document.getElementById('timer_total')
			if (elTotal !== null)
				elTotal.innerText = getters.timersTotal()

			// get active timer
			let timer = _find(state.timers, item => item.running)

			// check if timer is found
			if (timer !== undefined) {

				// get total timer of timer
				let timerTotal = getters.timerTotal(timer.uuid)

				// add title
				title = timerTotal + ' ' + timer.name + ' / '

				// sprint timer text
				let elTimerSprint = document.getElementById(timer.uuid + '_sprint')
				if (elTimerSprint !== null)
					elTimerSprint.innerText = getters.timerSprint(timer.uuid)

				// total timer text
				let elTimerTotal = document.getElementById(timer.uuid + '_total')
				if (elTimerTotal !== null)
					elTimerTotal.innerText = timerTotal

			}

			// set title to site
			document.title = title + 'Time Tracker'
		},
		// save timers to local storage
		timerSave(state) {
			localStorage.setItem(localStorageTimersKey, JSON.stringify(state.timers))
		},
		// save settings to local storage
		settingSave(state) {
			console.log(state.settings)
			localStorage.setItem(localStorageSettingsKey, JSON.stringify(state.settings))
		}
	},
	actions: {
		timerAdd({commit}, name) {
			commit('timerAdd', name)
			commit('timerSave')
		},
		timerRemove({commit}, uuid) {
			commit('timerRemove', uuid)
			commit('timerSave')
		},

		timerStart({commit}, uuid) {
			commit('timerStop')
			commit('timerStart', uuid)
			commit('timerSave')
			eventBus.emit('records', uuid)
		},
		timerStop({commit}, uuid) {
			commit('timerStop', uuid)
			commit('timerSave')
			eventBus.emit('records', uuid)
		},
		timerName({commit}, payload) {
			commit('timerName', payload)
			commit('timerSave')
		},

		timerSprintRemoveAll({commit}, uuid) {
			commit('timerSprintRemoveAll', uuid)
			commit('timerSave')
			eventBus.emit('records', uuid)
		},
		timerSprintRemoveIndex({commit}, payload) {
			commit('timerSprintRemoveIndex', payload)
			commit('timerSave')
			eventBus.emit('records', payload.uuid)
		},
		timerSprintRemoveLatest({commit}, uuid) {
			commit('timerSprintRemoveLatest', uuid)
			commit('timerSave')
			eventBus.emit('records', uuid)
		},

		timerStatusToggle({commit}, payload) {
			commit('timerStatusToggle', payload)
			commit('timerSave')
		},

		timerUpdateComment({commit}, payload) {
			commit('timerUpdateComment', payload)
			commit('timerSave')
		},

		settingSortChange({commit}, sort) {
			commit('settingSortChange', sort)
			commit('settingSave')
		},

		update({commit, getters}) {
			commit('update', {get: getters})
		}
	}
})
