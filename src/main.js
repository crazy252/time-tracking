import Vue from 'vue'
import App from './App.vue'
import store from './store'
import './registerServiceWorker'

// load fontawesome
import {library} from '@fortawesome/fontawesome-svg-core'
import {faPlus, faPlay, faPause, faTrash, faHistory, faUndo, faSave, faSortAlphaUp, faSortAlphaDown, faStar, faPen, faHeart} from '@fortawesome/free-solid-svg-icons'
import {FontAwesomeIcon} from '@fortawesome/vue-fontawesome'

// bind icons and component
library.add(faPlus, faPlay, faPause, faTrash, faHistory, faUndo, faSave, faSortAlphaUp, faSortAlphaDown, faStar, faPen, faHeart)
Vue.component('fai', FontAwesomeIcon)

// vue config
Vue.config.productionTip = false

// init vue
// eslint-disable-next-line
const app = new Vue({
	store,
	render: h => h(App)
}).$mount('#app')
